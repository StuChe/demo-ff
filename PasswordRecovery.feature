﻿@ModulePasswordRecovery
Feature: PasswordRecovery
	In order to not lose an existing account in the 'Cooper coin' application
	As a forgetful user of the 'Cooper coin' application 
	I want to be able to recover my password to the application

@p1 @smoke
Scenario: Recovery password to created user with valid data
	Given Recovery password page is open
	And New account was created
	| Login or Email        | Password      |
	| BillieJoe72@gmail.com | BillieJoeGD72 |
	When I fill in the fields 'Email/Login' with 'BillieJoe72@gmail.com', 'New password' with 'JoeGD72Billie' and 'Confirm password' with 'JoeGD72Billie' on the recovery password  page
	And I click the 'Enter' button
	And I get code from app 'Google autenticator'
	And I fill in the code field on 'Conformation' pop-up
	And I click 'Ok' button on 'Conformation' pop-up
	Then Message 'Password recovered successfully' displayed on recovery password page
	And Authorization to the 'Cooper coin' application is successful after password recovery

@p1
Scenario: App logo is displayed
	Given Password recovery page is open
	Then The coin logo is displayed on the password recovery page

@p2
Scenario Outline: Recovery password to created user with invalid data
	Given Recovery password page is open
	And New account was created
	| login or Email        | password      |
	| BillieJoe72@gmail.com | BillieJoeGD72 |
	When I fill in the fields 'Email/Login' with 'BillieJoe72@gmail.com', 'New password' with '<new password>' and 'Confirm password' with '<confirm password>' on the recovery password  page
	| New password   | Сonfirm password   |
	| <new password> | <confirm password> |
	And I click the 'Enter' button
	And I get code from app 'Google autenticator'
	And I fill in the code field on 'Conformation' pop-up
	And I click 'Ok' button on 'Conformation' pop-up
	Then Under field '<field>' displayed message '<message>'
Examples:
	| new password      | confirm password  | message                        | field            |
	| BillieJoeGD72#$   | BillieJoeGD72#$   | *Invalid password. Try again   | New password     |
	| Billie            | Billie            | *Invalid password. Try again   | New password     |
	| BillieJoeGD721012 | BillieJoeGD721012 | *Invalid password. Try again   | New password     |
	|                   |                   | *Invalid password. Try again   | New password     |
	| JoeGD72Billie     | BillieJoeGD72     | *Incorrect password. Try again | Confirm password |

@p3
Scenario: Go to the registration page
	Given Recovery password page is open
	When I click the button 'Registration'
	Then Registration page is open

@p3
Scenario: Go to the autorization page
	Given Recovery password page is open
	When I click the button 'Did you remember your password?'
	Then Autorization page is open