﻿@ModulLogOut
Feature: Log out from application
	In order to close work with application
	I am as user
	I want to be able to log out from system

Background:
	Given User is registred in app 'Copper Coin'
	And User is authorized

@p1
Scenario: It is possible to log out from App
	When I click to round button with first letter from your name in the upper right corner of application
	And I click in dropdown to 'Log out'
	And I click to button 'Yes' at opens popup 'Do you want to log out?'
	Then Authorization page is displayed

@p2
Scenario: It is possible to refuse to exit the application
	When I click to button in the upper right corner of aplication
	And I click in dropdown to 'Log out'
	And I click to button 'No' at opens popup 'Do you want to log out?'
	Then User is on the main page of the application