﻿@ModulePremiumAccount @UI
Feature: Premuim account
	In order to use additional functionality of the application
	As am a web application user
	I want to upgrade my account to premium

Background:
	Given User is authorized
	And The main page of the site is open

@p1 @smoke
Scenario: Update account to premium
	When I click on button 'Premium account'
	And I click on button 'Buy' on popup 'Premium account'
	Then Button 'Premium account' changed to button 'Edit user list'

@p1 @smoke
Scenario Outline: Add new user to group with correct data when you have premium account
	Given 
	And Users with '<usersEmail>' was exist in system
	And Authorized user have premium account
	When I click button 'Edit user list'
	And I click button 'Add new user' on popup 'Edit user list'
	And I enter '<usersEmail>' to field 'To add a new user, enter his email'
	Then Displayed popup 'User added successfully!'
Examples:
	| usersEmail       |
	| test@test.test   |
	| tokfd@gmail.coco |
	| 1231@gsd.c       |

@p2
Scenario Outline: Remove user from group when you have premium account
	Given Authorized user have premium account
	And Authorized user create group
	And Users with login 'Test1', 'Test_23', 'Test12', 'Testdsd', "bibaa", "boba" was added  in group
	When I click button 'Edit user list'
	And  I put a tick in <some> random checkbox near users login on popupp 'Edit user list'
	And  I click button 'Remove user' on popupp 'Edit user list'
	Then Displayed popup 'User(-s) removed successfully!'
Examples:
	| some |
	| 1    |
	| 3    |
	| 6    |
	| 10   |

@p3
Scenario:  Popup 'Premium account' is closed after clicking the 'X' button
	When I click on button 'Premium account'
	And I press button 'X' on popup 'Premium account'
	Then Popup 'Premium account' is not displayed on home page