﻿@AutorizationModule
Feature: Autorization
	In order to track my expenses
	As a user of the 'Cooper coin' application
	I want to be able to log in to the application

@p1 @smoke
Scenario Outline: Authorization with valid data
	Given Autorization page is open
	And New account was created
	| Login or Email   | Password   |
	| <login or Email> | <password> |
	When I fill in the fields 'Login or Email' with '<login or email>' and 'Password' with '<password>' on the autorization page
	| Login or Email   | Password   |
	| <login or Email> | <password> |
	And I click the 'Enter' button
	And I get code from app 'Google autenticator'
	And I enter google autenticator code in code field on 'Conformation' pop-up
	And I click 'Ok' button on 'Conformation' pop-up
	Then Home page is open
Examples:
	| login or email        | password         |
	| BillieJoe72@gmail.com | BillieJoeGD72    |
	| BillieJoe75@gmail.com | BillieJ          |
	| BillieJoe76@gmail.com | BillieJo         |
	| BillieJoe77@gmail.com | BillieJoeGD7210  |
	| BillieJoe78@gmail.com | BillieJoeGD72101 |
	| BillieJoe72           | BillieJoeGD72    |
	| Bill                  | BillieJoeGD72    |
	| Billli                | BillieJoeGD72    |
	| BillieJoe73           | BillieJoeGD72    |
	| BillieJoe721          | BillieJoeGD72    |
	| BillieJoe76           | BillieJ          |
	| BillieJoe77           | BillieJo         |
	| BillieJoe78           | BillieJoeGD7210  |
	| BillieJoe79           | BillieJoeGD72101 |

@p1
Scenario Outline: Authorization with invalid data
	Given Autorization page is open
	And New account was created
	| Login or Email   | Password   |
	| <login or Email> | <password> |
	When I fill in the fields 'Login or Email' with '<login or email>' and 'Password' with '<password>' on the autorization page
	| Login or Email   | Password   |
	| <login or Email> | <password> |
	And I click the 'Enter' button
	And I get code from app 'Google authenticator'
	And I enter google autenticator code in code field on 'Conformation' pop-up
	And I click 'Ok' button on 'Conformation' pop-up
	Then An error message 'You confused something. Try again' is displayed above the field 'Login or Email'
Examples:
	| login or email        | password          |
	| BillieJoe72gmail.com  | BillieJoeGD72     |
	| BillieJoe72@          | BillieJoeGD72     |
	| @gmail.com            | BillieJoeGD72     |
	| BillieJoe73@gmail.com | BillieJoeGD72#$   |
	| BillieJoe74@gmail.com | Billie            |
	| BillieJoe79@gmail.com | BillieJoeGD721012 |
	|                       | BillieJoeGD72     |
	| BillieJoe80@gmail.com |                   |
	| Bil                   | BillieJoeGD72     |
	| BillieJoe7210         | BillieJoeGD72     |
	| BillieJoe74           | BillieJoeGD72#$   |
	| BillieJoe75           | Billie            |
	| BillieJoe80           | BillieJoeGD721012 |
	|                       | BillieJoeGD72     |
	| BillieJoe81           |                   |

@p1
Scenario: Authorization with incorrect Google authenticator code
	Given Autorization page is open
	And New account was created
	| Login or Email       | Password      |
	| BillieJoe72gmail.com | BillieJoeGD72 |
	When I fill in the fields 'Login or Email' with 'BillieJoe72gmail.com' and 'Password' with 'BillieJoeGD72' on the autorization page
	And I click the 'Enter' button
	And I get code '123456' from app 'Google authenticator'
	And I enter google autenticator code in code field on 'Conformation' pop-up
	And I click 'Ok' button on 'Conformation' pop-up
	Then An error message '*Incorrect code. Try again' is displayed under the field 'Enter the code from Google authenticator'

@p1
Scenario: App logo is displayed
	Given Autorization page is open
	Then The coin logo is displayed on the autorization page

@p1
Scenario: Go to the registration page
	Given Autorization page is open
	When I click the button 'Registration'
	Then Registration page is open