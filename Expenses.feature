﻿@ModulExpenses @DiagramOfExpenses
Feature: Diagram of expenses
	 In order to better visual sense of expenses
	 As a user Copper Coin App 
	 I want to see diagram of expenses and interact with it

Background:
	Given User is registered
	And User is authorized
	And The main page of the app is open

@p1
Scenario Outline: Add first category to diagram of expenses
	Given Diagram of expenses is empty and displayed 'Your diagram is empty! Add expenditure'
	When I press button 'Add expenditure' on Home page
	And I press button 'Add category' on popup 'Add expenditure'
	And I enter <data> in field 'Category'
	And I press button 'Add' on popup 'Add category'
	Then Category with name <data> displayed on diagram of expenses
	And Category with name have someone color on diagram of expenses
	And The category displayed the percentage equivalent of the amount added to this category
	
Examples:
	| data    |
	| Test    |
	| 32131   |
	| %^($$#  |
	| t1#     |
	| {empty} |
	| выфвф   |

@p1 @smoke
Scenario Outline: Add category to diagram of expenses
	Given Diagram have one category with name 'First'
	When I press button 'Add expenditure' on Home page
	And I press button 'Add category' on popup 'Add expenditure'
	And I enter in field 'Category' <data>
	And I press button 'Add' on popup 'Add category'
	And I press button 'Add category' on popup 'Add expenditure'
	And I enter in field 'Category' <data2>
	And I press button 'Add' on popup 'Add category'
	Then Category with name 'First', <data> and <data2> displayed on diagram of expenses
Examples:
	| data    | data2   |
	| Test    | dsadsa  |
	| 32131   | {empty} |
	| %^($$#  | 123     |
	| t1#     | test    |
	| {empty} | &*@#$   |
	| выф     | 3вфы    |

@p1 @smoke
Scenario: All category on diagram of expenses clickable
	Given Diagram have some categories with name 'Test1', 'test','2313','$%&@#'
	And 'Test1' have some data about expenditure
	And 'test' have some data about expenditure
	And '2313' have some data about expenditure
	And '%&@#'' have some data about expenditure
	When I click on a category with a color that match the category 'Test1' on diagram of expenses
	Then On block 'List of expenses' displayed some data about category 'Test1'
	And I click on a category with a color that match the category 'test'
	Then On block 'List of expenses' displayed some data about category 'Test1'
	And I click on a category with a color that match the category '2313'
	Then On block 'List of expenses' displayed some data about category 'Test1'
	And I click on a category with a color that match the category '$%&@#'
	Then On block 'List of expenses' displayed some data about category 'Test1'
	And Each category can be clicked

@p2
Scenario Outline: After added expense to category size of category on diagram of expenses increases
	Given Diagram have some categories with name 'Test1', 'test','2313','$%&@#'
	And Categories 'Test1' have next data about expenses
	| Icon              | title | date       | money | login    | mandatory or optional expense |
	| selected checkbox | vodka | 01.01.1001 | 1000$ | koka1321 | Mandatory                     |
	| selected checkbox | milk  | 11.09.2021 | 100$  | test1    | Mandatory                     |
	And All categories have some data about expenditure
	And Opened popup 'Add expendicute'
	When I enter '<name>' to field 'Title'
	And I enter '<sum>' to field 'Expenditure'
	And I enter '<date>' to field 'Date'
	And I select 'Test1' in dropdown list category
	And I click on button 'Add' on popup 'Add expendicute'
	Then Size category 'Test1' increased on diagram of expenses by a percentage equivalent to the added expense
	And Amount of all added expenses in category is displayed on diagram of expenses
Examples:
	| name    | sum                | date       |
	| Test    | 100                | 12/01/2021 |
	| 32131   | 1                  | 12/01/20   |
	| %^($$#  | 13                 | 01/12/2021 |
	| t1#     | 12324123           | 07/12/00   |
	| {empty} | 10                 | 19/09/1999 |
	| выфвф   | 123456790123456790 | 13/13/13   |

@p2
Scenario Outline: Sum of added expenses can`t be zero or less
	Given Diagram of expenses have some category
	And opened popup 'Add expendicute'
	When I enter any text to field 'Title'
	And I enter '<sum>' to field 'Expenditure'
	And I enter any date to field 'Date'
	And I select any category in dropdown list
	And I click on button 'Add' on popup 'Add expendicute'
	Then Error message 'Expenditure can`t be zero or less'
Example:
	| sum   |
	| 0     |
	| 0.01  |
	| -0.01 |
	| -1    |

@p2
Scenario: After delete expense from category size of category on diagram of expenses decreased
	Given Diagram have some categories with name 'Test1', 'test','2313','$%&@#'
	And Categories 'Test1' have next data about expenses diagram
	| Icon              | title | date       | money | login    | mandatory or optional expense |
	| selected checkbox | vodka | 01.01.1001 | 1000$ | koka1321 | Mandatory                     |
	| selected checkbox | milk  | 11.09.2021 | 100$  | test1    | Mandatory                     |
	And All categories have some data about expenditure
	And Categoty 'Test1' takes 35% on di
	When I click on a category with a color that match the category 'Test1' on diagram of expenses
	And I select  expense with title 'milk'  with checkbox in row 'Icon'
	And I click button 'Remove expenditure' 
	Then Size category 'Test1' redused on diagram of expenses by a percentage equivalent to the added expense

@p3
Scenario: After changes in expense data relation percentage of categories is changing
	Given Diagram have some categories with name 'Test1', 'test','2313','$%&@#'
	And All categories have some data about expenditure
	And Category 'Test1' takes '35%' of the entire category on diagram of expenses
	When I add expense to category 'Test1' with some data 
	And I remove expenditure from category '2313' with checkbox in row 'Icon' and click 'Remove expenditure'
	Then The percentage of the "Test1" category changed from "35%" to another
	
@ModulExpenses @AddExpenditureButton
Feature: Add expenditure
	In order to keep track of yourself expenses
	As an application user
	I want to be able to get the amount of money spent, name, category and date of purchase

Background:
	Given User is registred in app 'Copper Coin'
	And User is authorized
	And Open popup 'Add expenditure' at home page of application

@p1 @smoke
Scenario Outline: Add expenditure in the list of expenses
	When I fill in the fields on pop-up 'Add expenditure'
	| Title   | Expediture   | Date   | Category   |
	| <title> | <expediture> | <date> | <category> |
	And I click to button 'Add' on the lower part of pop-up 'Add expenditure'
	Then Expenditure with title, amount, date and category is add to list of expenses
Examples: 
	| title                | expediture       | date       | category         |
	| Weekend in spa hotel | 300              | 25/10/2021 | Recreation       |
	| water supply         | 58.99            | 24/10/2021 | Utility bills    |
	| drainage             | 51,011           | 24/10/2021 | Utility bills    |
	| 2 bread              | 3,77             | 23/10/2021 | Conductive goods |
	| &*#]\%$@^(           | 1000000000000000 | 22/10/2021 | Services         |
	| 123456789            | test             | 26/10/2021 | Conductive goods |
	| билет на поезд       | 10               | 26/10/2021 | Recreation       |
	| iphone               | 1000             | 01/01/1991 | Conductive goods |
	| toys                 | 50               | 01/01/2776 | Conductive goods |
	| empty field          | 1                | 26/10/2021 | Services         |
	| test                 | empty field      | 26/10/2021 | Services         |

@ModulExpenses @TrashButton
Feature: Remove expenses
	In order to delete unnecessary expenses 
	I am as a web application user
	I want to remove expenditure from my list of expenditure

Background:
	Given User is authorized
	And The main page of the site is open
	And The 'List of expenses' field is open
	And Field 'List of expenses' filled with 10 expenditure

@p1 @smoke
Scenario Outline: Correct delete expenditure
	When I select <any> expenditure in 'List of expenses'
	And I press on button 'Trash can' under field 'List of expenses'
	Then Selected expenditure was deleted
Examples:
	| any |
	| 1   |
	| 4   |
	| 10  |

@ModulExpenses @ListOfExpenses
Feature: List of expenses
	In order to quicly get information about all expenses
	As a user Copper Coin App 
	I want to added expenses is displayed in the list of expenses for all period or chooses date

Background: 
	Given User is registed
	And User is autorized

@p2
Scenario: 'List of expenses' and 'Statistic of expenses' is empty for first time
	Then On homepage displayed empty field 'List of expenses'
	And On homepage displayed empty field 'Statistic of expenses'
	And Button 'Show expenses' don`t active
	And Default date is current month and current year on dropdown