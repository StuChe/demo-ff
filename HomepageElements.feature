﻿@ModulHomePage @UI
Feature: Visual elements
	In order to using of application be comfortable, understanded and recognizable
	I am as an application user
	I want to be able to see the logo, the name, the slogan of the applicationon and colored icons with categories of expenses at the home page

Background:
	Given User is registred in app 'Copper Coin'
	And User is authorized

@p2
Scenario: See logo, name of application and slogan
	Then The logo is gold coin displayed in the upper left corner of the screen
	And The name of application with slogan is 'Copper coin - don`t waste your money' close to  logo

@p3
Scenario: See colored icons with categories of expenses
	Then The orange icons with category of expenses 'Conductive goods' is displayed in the middle of the screen
	And The yellow icons with category of expenses 'Utility bills' is displayed in the middle of the screen
	And The green icons with category of expenses 'Recreation' is displayed in the middle of the screen
	And The blue icons with category of expenses 'Services' is displayed in the middle of the screen
