﻿@ModulCurrentBudget
Feature: Add budget
	In order to keep track of yourself finances
	I am as an application 'Copper Coin' user
	I want to be able to add of money get into budget and see progress bar 'Current budget'

Background:
	Given User is registred in app 'Copper Coin'
	And User is authorized

@p1
Scenario:
	And User don`t added any butget before
	Then Progress bar 'Current budget' is empty

@p1 @smoke
Scenario Outline: Update budget with valid data
	When I click to button 'Update budget' at home page
	And I enter '<data>' in in the field 'Enter the amount you want to add to the budget' on popup 'Update budget'
	And I click to button 'Add' on popup 'Update budget'
	Then Message 'Amount was successfully added' appears
	And Progress bar 'Current budget' is filled for the equivalent of the added expense
	And Under progress bar displayed 'Current budget: <sum all added budget>$'
Examples: 
	| data         |
	| 100          |
	| 0            |
	| 0.50         |
	| 100,99       |
	| 999999999999 |

@p2
Scenario Outline: Update budget with no valid data
	When I click to button 'Update budget' at home page
	And I fill in the field 'Enter the amount you want to add to the budget' with no valid <data> in opens popup 'Update budget'
	And I click to button 'Add'
	Then Message of red colore  is'*Incorrect value. Try againe' under field for amount
Examples: 
	| data       |
	| -0         |
	| -1         |
	| empty field|
	| test       |
	| */-+%&@#$? |
	| 2+2        |
	| восемь     |

@p2
Scenario Outline: After add expenses progress bar is the progress bar of the 'Current budget' is decreased by a percentage equivalent to the added expense and can`t be less than zero
	And Budget more than <expenditure>
	When I click to button 'Add expenditure'
	And I add '<title>' and '<expenditure>' in opened popup'Add expenditure'
	Then Progress bar of the 'Current budget' is decreased by a percentage equivalent to the added expense
Examples: 
	| title  | expenditure |
	| TestBt | 10          |
	| TestZr | 0           |
	| TestLn | -1          |
