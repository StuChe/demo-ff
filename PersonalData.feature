﻿@ModulePersonalData
Feature: PersonalData
	In order to keep personal data up-to-date
	As a user of the 'Cooper coin' application
	I want to be able to change my personal data

@p1 @smoke
Scenario Outline: User has the ability to edit personal data
	Given User created and logged in
	| First name | Last name  | Login       | Email                 | Phone      | Password      |
	| Billie-Joe | Arm-strong | BillieJoe72 | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72 |
	And Main page opened
	When I click to round button with first letter from your name in the upper right corner of application
	And I click in dropdown 'Personal data'
	And I fill in popup the fields '<field>' with '<data>', 'New password' with '<new password>', 'Confirm password' with '<confirm password>' on the personal data window
	| Field   | Data   | New password   | Confirm password   |
	| <field> | <data> | <new password> | <confirm password> |
	And I click the 'Confirm' button in 'Personal data' window
	Then Message 'Setting saved successfully' is displayed
	And Changes in personal data is saved
Examples:
	| field            | data                    | new password  | confirm password |
	| First name       | Billie                  |               |                  |
	| Last name        | Strong-arm              |               |                  |
	| Login            | Joe72Billie             |               |                  |
	| Email            | BillieJoe1984@gmail.com |               |                  |
	| Phone            | 0987654321              |               |                  |
	| Current password | BillieJoeGD72           | GordonDestr72 | GordonDestr72    |

@p2
Scenario Outline: Impossible to save personal data with invalid data
	Given User created and logged in
	| First name | Last name  | Login       | Email                 | Phone      | Password      |
	| Billie-Joe | Arm-strong | BillieJoe72 | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72 |
	And Main page opened
	When I click to round button with first letter from your name in the upper right corner of application
	And I click in dropdown 'Personal data'
	When I fill in popup the fields '<field>' with '<data>', 'New password' with '<new password>', 'Confirm password' with '<confirm password>' on the personal data window
	| Field   | Data   | New password   | Confirm password   |
	| <field> | <data> | <new password> | <confirm password> |
	And I click the 'Confirm' button in 'Personal data' window
	Then Error message 'Invalid <field>. Try again' is displayed under the field '<field>'
	| field            | message                               |
	| First name       | Invalid First name. Try again         |
	| Last name        | Invalid Last name. Try again          |
	| Login            | Invalid Login. Try again              |
	| Email            | Invalid Email. Try again              |
	| Phone            | Invalid Phone. Try again              |
	| Current password | Incorrect Current password. Try again |
	| New password     | Invalid New password. Try again       |
	| Confirm password | Incorrect Confirm password. Try again |
	And Changes in personal data is not saved
Examples:
	| field            | data          | new password | confirm password |
	| First name       | ----------    |              |                  |
	| Last name        | -Arm-strong-  |              |                  |
	| Login            | Bil           |              |                  |
	| Email            | BillieJoe72@  |              |                  |
	| Phone            | 098           |              |                  |
	| Current password | BillieJoeGD72 | Billie       | Billie           |