﻿@ModuleRegistration
Feature: Registration
	In order to track my expenses
	As a user of the 'Cooper coin' application
	I want to be able to register on the application

@p1 @smoke
Scenario Outline: Register with valid data
	Given Registration page is open
	When I fill in Registration fields
	| First name   | Last name   | Login   | Email   | Phone   | Password   | Confirm password |
	| <first name> | <last name> | <login> | <email> | <phone> | <password> | <password>       |
	And I confirm the registration by clicking the button 'Registration'
	Then Pop-up 'Successful registration' is displayed on registration page
	And On pop-up 'Successful registration' displayed code for app 'Google authenticator'
Examples:
	| first name                               | last name                                | login        | email                 | phone      | password         | confirm password |
	| Billie-Joe                               | Arm-strong                               | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| B                                        | Arm-strong                               | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| Bi                                       | Arm-strong                               | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| Billie-Joe11121314151617181920212223242  | Arm-strong                               | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| Billie-Joe111213141516171819202122232425 | Arm-strong                               | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| Billie-Joe                               | A                                        | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| Billie-Joe                               | Ar                                       | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| Billie-Joe                               | Arm-strong11121314151617181920212223242  | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| Billie-Joe                               | Arm-strong111213141516171819202122232425 | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| Billie-Joe                               | Arm-strong                               | Bill         | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| Billie-Joe                               | Arm-strong                               | Billi        | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| Billie-Joe                               | Arm-strong                               | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| Billie-Joe                               | Arm-strong                               | BillieJoe721 | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72    | BillieJoeGD72    |
	| Billie-Joe                               | Arm-strong                               | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJ          | BillieJ          |
	| Billie-Joe                               | Arm-strong                               | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJo         | BillieJo         |
	| Billie-Joe                               | Arm-strong                               | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD7210  | BillieJoeGD7210  |
	| Billie-Joe                               | Arm-strong                               | BillieJoe72  | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72101 | BillieJoeGD72101 |

@p1
Scenario Outline: Register with invalid data
	Given Registration page is open
	When I fill in Registration fields
	| First name   | Last name   | Login   | Email   | Phone   | Password   | Confirm password |
	| <first name> | <last name> | <login> | <email> | <phone> | <password> | <password>       |
	And I confirm the registration by clicking the button 'Registration'
	Then Error message 'Invalid <field>. Try again' is displayed under the field
	| Field            |
	| First name       |
	| Last name        |
	| Login            |
	| Email            |
	| Phone            |
	| Password         |
	| Confirm password |
Examples:
	| first name                                | last name                                 | login         | email                 | phone      | password          | confirm password  |
	| ----------                                | Arm-strong                                | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| -Billie-Joe                               | Arm-strong                                | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe-                               | Arm-strong                                | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| -Billie-Joe-                              | Arm-strong                                | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	|                                           | Arm-strong                                | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe1112131415161718192021222324252 | Arm-strong                                | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | ----------                                | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | -Arm-strong                               | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | Arm-strong-                               | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | -Arm-strong-                              | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                |                                           | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | Arm-strong1112131415161718192021222324252 | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | Arm-strong                                | Bil           | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | Arm-strong                                | BillieJoe7210 | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | Arm-strong                                | BillieJoe72   | BillieJoe72gmail.com  | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | Arm-strong                                | BillieJoe72   | BillieJoe72@          | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | Arm-strong                                | BillieJoe72   | @gmail.com            | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | Arm-strong                                | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72#$   | BillieJoeGD72#$   |
	| Billie-Joe                                | Arm-strong                                | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | Billie            | Billie            |
	| Billie-Joe                                | Arm-strong                                | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD721012 | BillieJoeGD721012 |
	| Billie-Joe                                | Arm-strong                                | BillieJoe72   | BillieJoe72@gmail.com |            | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | Arm-strong                                |               | BillieJoe72@gmail.com | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | Arm-strong                                | BillieJoe72   |                       | 1234567890 | BillieJoeGD72     | BillieJoeGD72     |
	| Billie-Joe                                | Arm-strong                                | BillieJoe72   | BillieJoe72@gmail.com | 1234567890 |                   |                   |

@p1
Scenario: App logo is displayed
	Given Registration page is open
	Then The coin logo is displayed on the registration page

@p2
Scenario: Go to the login page
	Given Registration page is open
	When I click the button 'Autorization'
	Then Autorization page is open